import { defineStore } from "pinia";
import { useStorage } from "@vueuse/core";

const numbers = {
  b: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
  i: [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
  n: [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45],
  g: [46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60],
  o: [61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75],
};

function randomNumbers(key) {
  let unselected = [...numbers[key]];
  let selected = [];
  let numberCount = key == "n" ? 4 : 5;

  for (let i = 0; i < numberCount; i++) {
    let unselectedIndex = Math.floor(Math.random() * unselected.length);

    selected.push(unselected[unselectedIndex]);

    if (unselectedIndex !== -1) {
      unselected.splice(unselectedIndex, 1);
    }
  }

  return selected;
}

export const useStore = defineStore("main", {
  state: () => ({
    cards: useStorage("cards/v1", []),
    selectedNumbers: useStorage("selectedNumbers/v1", []),
    darkMode: useStorage(
      "darkMode/v1",
      window.matchMedia("(prefers-color-scheme: dark)").matches
    ),
  }),
  getters: {
    hasNoCards() {
      return !this.cards.length;
    },
    isDarkMode() {
      return this.darkMode;
    },
  },
  actions: {
    generateCards(count) {
      let cards = [];

      for (let i = 0; i < count; i++) {
        let card = {
          b: randomNumbers("b"),
          i: randomNumbers("i"),
          n: randomNumbers("n"),
          g: randomNumbers("g"),
          o: randomNumbers("o"),
        };

        cards.push(card);
      }

      this.cards = cards;
    },
    toggle(number) {
      if (!this.selectedNumbers.includes(number)) {
        this.selectedNumbers.push(number);
      } else {
        let index = this.selectedNumbers.indexOf(number);

        if (index !== -1) {
          this.selectedNumbers.splice(index, 1);
        }
      }
    },
    toggleDarkMode() {
      this.darkMode = !this.darkMode;
    },
    reset() {
      this.cards = [];
      this.selectedNumbers = [];
    },
  },
});
